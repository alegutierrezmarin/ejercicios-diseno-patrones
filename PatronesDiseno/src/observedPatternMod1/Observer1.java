package observedPatternMod1;

public class Observer1 implements IObserver {
    @Override
    public void update(int i) {
        System.out.println("Observador 1: el valor de Subject ahora es : " + i);
    }
}
