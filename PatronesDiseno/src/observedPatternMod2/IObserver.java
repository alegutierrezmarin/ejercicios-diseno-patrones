package observedPatternMod2;

public interface IObserver {
    void update(String d, int i);
}
