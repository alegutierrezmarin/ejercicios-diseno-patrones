package observedPatternMod2;

public class Observer1 implements IObserver {
    @Override
    public void update(String d, int i) {

        System.out.println("Observer1: myValue in" + d + " is now: " + i);

    }
}
