package observedPatternMod1;

import java.util.ArrayList;
import java.util.List;

public class Subject implements ISubject {

    private int myValue;
    List<IObserver> observerList = new ArrayList<IObserver>();

    public int getMyValue() {
        return myValue;
    }

    public void setMyValue(int myValue) {
        this.myValue = myValue;
        // notifica a los observadores
        notifyObservers(myValue);
    }

    @Override
    public void register(IObserver o) {
        observerList.add(o);
    }

    @Override
    public void unregister(IObserver o) {
        observerList.remove(o);
    }

    @Override
    public void notifyObservers(int updateValue) {
        for (int i = 1; i < observerList.size(); i++) {
            observerList.get(i).update(updateValue);
        }
    }

}
