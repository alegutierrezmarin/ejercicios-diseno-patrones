package observedPatternMod1;

interface IObserver {
    void update(int i);
}
