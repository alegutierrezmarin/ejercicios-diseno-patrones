package observedPatternMod1;

public class ObservedPatternMod1 {
    public static void main(String[] args) {

        System.out.println("******Version modificada del patron observador******");
        Subject s1 = new Subject();
        Observer1 o1 = new Observer1();
        Observer2 o2 = new Observer2();

        s1.register(o2);
        // s1.register(o2);

        s1.setMyValue(5);
        System.out.println("-------------------");
        s1.setMyValue(25);
        System.out.println("-------------------");

        // anulamos registro al o1 solamente
        s1.unregister(o1);
        // ahora solamente el o2 observa los cambio
        s1.setMyValue(100);
    }
}
