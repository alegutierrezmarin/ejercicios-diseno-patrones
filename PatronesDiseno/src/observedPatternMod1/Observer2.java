package observedPatternMod1;

public class Observer2 implements IObserver {
    @Override
    public void update(int i) {
        System.out.println("Observador 2: observers->myValue a cambiado en el sujeto a: " + i);
    }
}
