package observedPatternMod2;

import java.util.ArrayList;
import java.util.List;

public class Subject1 implements ISubject {
    private int myValue;

    public int getMyValue() {
        return myValue;
    }

    public void setMyValue(int myValue) {
        this.myValue = myValue;
        notifyObservers(myValue);
    }

    List<IObserver> observersList = new ArrayList<IObserver>();

    @Override
    public void register(IObserver observer) {
        observersList.add(observer);
    }

    @Override
    public void unregister(IObserver observer) {
        observersList.remove(observer);
    }

    @Override
    public void notifyObservers(int updateValue) {
        for (int i = 0; i < observersList.size(); i++) {
            observersList.get(i).update(this.getClass().getSimpleName(), updateValue);
        }
    }
}
