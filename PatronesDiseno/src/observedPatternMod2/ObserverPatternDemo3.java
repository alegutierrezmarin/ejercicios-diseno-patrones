package observedPatternMod2;

public class ObserverPatternDemo3 {
    public static void main(String[] args) {
        System.out.println("---------------DEMO PAttern Observer 3 ------------------");
        Subject1 s1 = new Subject1();

        Observer1 o1 = new Observer1();

        s1.register(o1);

    }
}
