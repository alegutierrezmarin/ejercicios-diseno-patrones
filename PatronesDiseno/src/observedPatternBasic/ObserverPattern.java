package observedPatternBasic;

public class ObserverPattern {

    public static void main(String[] args) {
        System.out.println("****PATRON OBSERVADOR DEMO***");
        Observer o1 = new Observer();
        Subject sub1 = new Subject();

        sub1.register(o1);

        System.out.println("Setting Bandera = 5");
        sub1.set_flag(5);

        System.out.println("Setting Bandera = 25");
        sub1.set_flag(25);

        sub1.unregister(o1);

        // notificacion de que el objeto 1 se ha desregistrado

        System.out.println("Setting bandera = 50");
        sub1.set_flag(50);
    }
}
